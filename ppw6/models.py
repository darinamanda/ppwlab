from django.db import models
from django.utils import timezone

# Create your models here.

class HowAre(models.Model):
    how_are = models.CharField(max_length = 300)
    submit_time = models.DateTimeField(default=timezone.now())
