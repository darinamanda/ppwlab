from django.urls import path
from .views import *

app_name = 'ppw6'
urlpatterns = [
	path('', profile, name = 'default'),
	path('story/', landing_page, name='how'),
	path('home/', profile, name='profile'),
]