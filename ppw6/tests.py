from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
#Cek URLnya ada atau ga
class PPW6Test(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/ppw6/story/')
        self.assertEqual(response.status_code, 200)
#Cek pake template landingpage.html ga
    def test_landing_page_using_landing_page_template(self):
        response = Client().get('/ppw6/story/')
        self.assertTemplateUsed(response, 'landingpage.html')
#Cek manggil functionnya bener atau ga
    def test_landing_page_using_landing_page_func(self):
        found = resolve('/ppw6/story/')
        self.assertEqual(found.func, landing_page)
#Cek modelnya kebuat atau ga
    def test_model_can_create(self):
        status = HowAre.objects.create(how_are = 'tired')
        counting_all_available_status = HowAre.objects.all().count()
        self.assertEqual(counting_all_available_status,1)
#cek kalo landing pagenya bisa render hasilnya
    def test_landing_page_can_render_result(self):
        fortest = 'oh no'
        post = Client().post('/ppw6/story/', {'how_are': fortest})
        self.assertEqual(post.status_code, 200)

        response = Client().get('/ppw6/story/')
        html_post = response.content.decode('utf8')
        self.assertIn(fortest, html_post)

class PPW6_Profile_Test(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get("/ppw6/home/")
        self.assertEqual(response.status_code, 200)
    def test_profile_using_profile_template(self):
        response = Client().get("/ppw6/home/")
        self.assertTemplateUsed(response, "profile.html")
    def test_profilee_using_profile_func(self):
        found = resolve("/ppw6/home/")
        self.assertEqual(found.func, profile)

class NewInputTestSelenium(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(NewInputTestSelenium, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(NewInputTestSelenium, self).tearDown()
    
    def test_can_start_a_list_and_retrieve_it_later(self):
        self.selenium.get('http://ppw-darina.herokuapp.com/ppw6/story/')
        time.sleep(3) # Let the user actually see something!
        input_box = self.selenium.find_element_by_name('how_are')
        input_box.send_keys('Coba Coba')
        input_box.submit()
        time.sleep(3) # Let the user actually see something!
        self.assertIn("Coba Coba", self.selenium.page_source)
    
    def test_landing_page_title(self):
        self.selenium.get('http://ppw-darina.herokuapp.com/ppw6/story/')
        self.assertIn('Your Online Diary', self.selenium.title)
    
    def test_landing_page_header_text(self):
        self.selenium.get('http://ppw-darina.herokuapp.com/ppw6/story/')
        header_text = self.selenium.find_element_by_tag_name('strong').text
        self.assertIn('Hello! How are you?', header_text)
        time.sleep(3)
    
    def test_landing_page_header_text_css_size(self):
        self.selenium.get('http://ppw-darina.herokuapp.com/ppw6/story/')
        header_text_size = self.selenium.find_element_by_tag_name('strong').value_of_css_property('font-size')
        self.assertIn('48px', header_text_size) #1 rem = 16px, 3 rem = 48px
        time.sleep(3)
    
    def test_landing_page_headrer_text_css_position(self):
        self.selenium.get('http://ppw-darina.herokuapp.com/ppw6/story/')
        header_text_position = self.selenium.find_element_by_tag_name('strong').value_of_css_property('text-align')
        self.assertIn('center', header_text_position)
        time.sleep(3)


    
