from django import forms

class HowAre_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    how_are = forms.CharField(label = '', max_length = 300, required=True, widget=forms.TextInput(attrs=attrs))
