from django.shortcuts import render, redirect
from .forms import HowAre_Form
from .models import HowAre

# Create your views here.
def landing_page(request):
    form = HowAre_Form(request.POST or None)
    if(request.method == 'POST'):
        result = HowAre.objects.all()
        response = {
            "result" : result
        }
        response['form'] = form
        if(form.is_valid()):
            response['how_are'] = request.POST.get('how_are')
            HowAre.objects.create(how_are=response['how_are'])
            return render(request, 'landingpage.html', response)
        else:
            return render(request, 'landingpage.html', response)
    else:
        result = HowAre.objects.all()
        response = {
            "result" : result
        }
        response['form'] = form
        return render(request, 'landingpage.html', response)

def profile(request):
	return render(request, "profile.html", {})