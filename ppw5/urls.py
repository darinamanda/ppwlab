from django.urls import path
from .views import *

app_name = 'ppw5'
urlpatterns = [
    path('schedule/', schedule, name='schedule'),
    path('post/', activity_post, name='activity_post'),
    path('activity/', activity, name='activity'),
    path('activity/empty', delete, name='empty')
]