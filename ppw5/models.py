from django.db import models
from django.utils import timezone
# Create your models here.

class JadwalPribadi(models.Model):
    date_time = models.DateField(default = timezone.now)
    activity_name = models.CharField(max_length = 200)
    place_name = models.CharField(max_length = 50)
    time = models.TimeField()

    AKADEMIS = 'Akademis'
    KEPANITIAAN ='Kepanitiaan dan Organisasi'
    KELUARGA = 'Keluarga'
    OLAHRAGA = 'Olahraga'
    MAIN = 'Main'
    OTHER = 'Other'
    CATEGORY_CHOICES = (
        (AKADEMIS, 'Akademis'),
        (KEPANITIAAN, 'Kepanitiaan dan Organisasi'),
        (KELUARGA, 'Keluarga'),
        (OLAHRAGA, 'Olahraga'),
        (MAIN, 'Main'),
        (OTHER, 'Other'),
    )

    category_name = models.CharField(max_length = 70, choices = CATEGORY_CHOICES, default = AKADEMIS)
