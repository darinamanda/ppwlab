from django import forms

class Activity_Form(forms.Form):
    AKADEMIS = 'Akademis'
    KEPANITIAAN ='Kepanitiaan dan Organisasi'
    KELUARGA = 'Keluarga'
    OLAHRAGA = 'Olahraga'
    MAIN = 'Main'
    OTHER = 'Other'
    CATEGORY_CHOICES = (
        (AKADEMIS, 'Akademis'),
        (KEPANITIAAN, 'Kepanitiaan dan Organisasi'),
        (KELUARGA, 'Keluarga'),
        (OLAHRAGA, 'Olahraga'),
        (MAIN, 'Main'),
        (OTHER, 'Other'),
    )
    attrs = {
        'class': 'form-control'
    }

    date_time = forms.DateField(label = '', required=True, widget=forms.DateInput(attrs={'type': 'date'}))
    time = forms.TimeField(label = '', required=True, widget=forms.TimeInput(attrs={'type': 'time'}))
    activity_name = forms.CharField(label = '', max_length = 200, required=True, widget=forms.TextInput(attrs=attrs))
    place_name = forms.CharField(label = '', max_length = 50, required=True, widget=forms.TextInput(attrs=attrs))
    category_name = forms.CharField(max_length = 70, label = '', required=True, widget = forms.Select(choices = CATEGORY_CHOICES))