# Generated by Django 2.1.1 on 2018-10-03 17:22

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='JadwalPribadi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateTimeField(auto_now_add=True)),
                ('activity_name', models.CharField(max_length=200)),
                ('place_name', models.CharField(max_length=50)),
                ('category_name', models.CharField(choices=[('AK', 'Akademis'), ('KOR', 'Kepanitiaan'), ('KLG', 'Keluarga'), ('OR', 'Olahraga'), ('MN', 'Main'), ('OT', 'Other')], default='AK', max_length=2)),
            ],
        ),
    ]
