from django.shortcuts import render
from .forms import Activity_Form
from .models import JadwalPribadi
from django.http import HttpResponseRedirect

# Create your views here.

def activity_post(request):
    response = {}
    form = Activity_Form(request.POST or None)
    print(form.errors)
    if(request.method == 'POST' and form.is_valid()):
        response['date_time'] = request.POST['date_time']
        response['time'] = request.POST['time']
        response['activity_name'] = request.POST['activity_name']
        response['place_name'] = request.POST['place_name']
        response['category_name'] = request.POST['category_name']
        print(request.POST["date_time"])
        formdone = JadwalPribadi(
            date_time=response['date_time'],
            time=response['time'],
            activity_name=response['activity_name'],
            place_name=response['place_name'],
            category_name=response['category_name']
        )
        formdone.save()
        return HttpResponseRedirect('/ppw5/activity')
    else:
        return HttpResponseRedirect('/ppw5/schedule')

def schedule(request):
    response = {'form': Activity_Form}
    return render(request, 'actform.html', response)

def activity(request):
    result = JadwalPribadi.objects.all()
    response = {'result':result}
    return render(request, 'actformresult.html', response)

def delete(request):
    JadwalPribadi.objects.all().delete()
    return render(request, 'actformresult.html', {})

