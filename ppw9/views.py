from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth import logout as logout_bisa
import requests
import json

response = {}
# Create your views here.
def lab9(request):
	if request.user.is_authenticated and "like" not in request.session:
		request.session["sessionid"]=request.session.session_key
		request.session["like"]=[]
	return render(request, "listbook.html", {})

def data(request):
	try:
		q = request.GET['q']
	except:
		q = 'quilting'
	jsonyo = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()
	return JsonResponse(jsonyo)

@csrf_exempt
def like(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] not in lst :
            lst.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)    
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

@csrf_exempt
def unlike(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] in lst :
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)    
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")
    
def gotlike(request):
    if request.user.is_authenticated:
        if(request.method=="GET"):
            if request.session["like"] is not None:
                response["message"] = request.session["like"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)

def logout(request):
    logout_bisa(request)
    return redirect('/ppw9/book/')




