from django.urls import path
from .views import *

urlpatterns = [
	path('', lab9, name = 'homelab9'),
	path('book/', lab9, name='hi'),
    path('book/data/', data, name='data'),
    path('logout/', logout, name='logout'),
    path('book/like/', like, name='like'),
    path('book/unlike/', unlike, name='unlike'),
    path('book/gotlike/', gotlike, name='gotlike'),
]