from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest
from .views import *
# Create your tests here.

class PPW9Test(TestCase):
    def test_lab9_page_url_is_exist(self):
        response = Client().get('/ppw9/book/')
        self.assertEqual(response.status_code, 200)
    def test_lab9_page_using_lab9_page_template(self):
        response = Client().get('/ppw9/book/')
        self.assertTemplateUsed(response, 'listbook.html')
    def test_lab9_page_using_lab9_page_func(self):
        found = resolve('/ppw9/book/')
        self.assertEqual(found.func, lab9)
    def test_logout_success_redirected(self):
        response = self.client.get('/ppw9/logout/')
        self.assertEqual(response.status_code, 302)
