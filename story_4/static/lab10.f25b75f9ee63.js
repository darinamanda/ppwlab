function validation(){
    event.preventDefault(true);
    let fields = document.forms["subcriber_form"].getElementsByTagName("input");
    let csrf_token = fields[0].value;
    let name_input = fields[1].value;
    let email_input = fields[2].value;
    let password_input = fields[3].value;
    $.ajax({
        url: '/subscribe/checker/',
        type: POST,
        dataType: 'json',
        data: JSON.stringify({
            name : name_input,
            email : email_input,
            password : password_input
        }),
        beforeSend: function(request){
            request.setRequestHeader('X-CSRFTOKEN', csrf_token);
        },
        success: function(result){
            let alerttext = document.getElementById("alerttext");
            let submitbutton = document.getElementById("submitbutton");
            alerttext.classList.remove("red-text", "green-text");
            if(result['valid']){
                if(result['subscriber_exists']){
                    submitbutton.disabled = true;
                    alerttext.classList.add("red-text");
                    alerttext.innerText = "A user with this email address already exists.";
                }else{
                    submitbutton.disabled = false;
                    alerttext.classList.add("green-text");
                    alerttext.innerText = "Available! :)"
                }
            }else if(!result['valid']){
                submitbutton.disabled = true;
                alerttext.classList.add("red-text");
                alerttext.innerText("There are some errors, please correct them!")
            }
        }
    });
}

function userclick(){
    event.preventDefault(true);
    let form = document.forms["subcriber_form"]
    let fields = form.getElementsByTagName("input");
    let csrf_token = fields[0].value;
    let name_input = fields[1].value;
    let email_input = fields[2].value;
    let password_input = fields[3].value;
    $.ajax({
        url: '/subscribe/subscribe-success/',
        type: POST,
        dataType: 'json',
        data: JSON.stringify({
            name : name_input,
            email : email_input,
            password : password_input,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token);
        },
        complete: function(result) {
            let alerttext = document.getElementById("alerttext");
            alerttext.classList.remove("red-text", "green-text");
            alerttext.classList.add("green-text");
            alerttext.innerHTML = "Thank you for subscribing!";
            let submitbutton = document.getElementById("submitbutton");
            submitbutton.disabled = true;
            form.reset();
        }
    });
}