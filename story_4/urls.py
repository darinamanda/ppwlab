"""story_4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('landingpage.urls')),
    path('admin/', admin.site.urls),
    path('ppw4/', include('ppw4.urls')),
    path('ppw5/', include(('ppw5.urls', 'ppw5'), namespace='ppw5')),
    path('ppw6/', include(('ppw6.urls', 'ppw6'), namespace='ppw6')),
    path('ppw8/', include(('ppw8.urls', 'ppw8'), namespace='ppw8')),
    path('ppw9/', include(('ppw9.urls', 'ppw9'), namespace='ppw9')),
    path('landingpage/', include(('landingpage.urls', 'landingpage'), namespace='landingpage')),
    path('ppw10/', include(('ppw10.urls', 'ppw10'), namespace='ppw10')),
    path('auth/', include('social_django.urls', namespace ='social')),
]
