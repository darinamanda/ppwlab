
from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest
from .views import *
# Create your tests here.

class PPW9Test(TestCase):
    def test_awalbgt_page_url_is_exist(self):
        response = Client().get('/landingpage/welcome/')
        self.assertEqual(response.status_code, 200)
    def test_awalbgt_page_using_awalbgt_page_template(self):
        response = Client().get('/landingpage/welcome/')
        self.assertTemplateUsed(response, 'awalbgt.html')
    def test_lab9_page_using_lab8_page_func(self):
        found = resolve('/landingpage/welcome/')
        self.assertEqual(found.func, landing)