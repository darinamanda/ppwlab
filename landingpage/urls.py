from django.urls import path

from . import views

urlpatterns = [
    path('', views.landing, name='default'),
    path('welcome/', views.landing, name='welcome'),
]