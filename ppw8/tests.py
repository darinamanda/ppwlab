from django.test import TestCase
from django.test import Client
from django.urls import resolve
import unittest
from .views import *
# Create your tests here.

class PPW8Test(TestCase):
    def test_lab8_page_url_is_exist(self):
        response = Client().get('/ppw8/hi/')
        self.assertEqual(response.status_code, 200)
    def test_lab8_page_using_lab8_page_template(self):
        response = Client().get('/ppw8/hi/')
        self.assertTemplateUsed(response, 'lab8.html')
    def test_lab8_page_using_lab8_page_func(self):
        found = resolve('/ppw8/hi/')
        self.assertEqual(found.func, lab8)