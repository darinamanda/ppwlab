from django.urls import path
from .views import *

app_name = 'ppw8'
urlpatterns = [
	path('', lab8, name = 'default'),
	path('hi/', lab8, name='hi'),
]