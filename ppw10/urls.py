from django.urls import path
from . import views

urlpatterns = [
    path('', views.sub, name="index"),
    path('subscribe/', views.sub, name="index"),
    path('checker/', views.checker, name="check"),
    path('subscribe-success/', views.subscribe, name="subscribe"),
    path('subscriber-list/', views.subscriberList, name="subscriberList"),
    path('unsubscribe/', views.unsubscribe, name="unsubscribe"),
]
