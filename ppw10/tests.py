
from django.test import TestCase
from .models import Subscriber
from .views import (
    sub,
    checker,
    subscribe
)
from django.urls import resolve
import json
import requests

# Create your tests here.

class SubscribePageTest(TestCase):
    def test_subscribe_page_accessible(self):
        response = self.client.get('/ppw10/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_page_using_sub_function(self):
        found = resolve('/ppw10/subscribe/')
        self.assertEqual(found.func, sub)

    def test_subscribe_page_using_checker_function(self):
        found = resolve('/ppw10/checker/')
        self.assertEqual(found.func, checker)
    
    def test_subscribe_page_using_subscribe_function(self):
        found = resolve('/ppw10/subscribe-success/')
        self.assertEqual(found.func, subscribe)
    
    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="aduh", email="bisadong@email.com", password="haihai1234")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)

    def test_subscribe_page_uses_correct_template(self):
        response = self.client.get('/ppw10/subscribe/')
        self.assertTemplateUsed(response, 'lab10.html')
        
class UserModelTest(TestCase):
    def test_default_attribute(self):
        user = Subscriber()
        self.assertEqual(user.name, '')
        self.assertEqual(user.email, '')
        self.assertEqual(user.password, '')


class SubscribeCheckingTest(TestCase):
    def test_post_ajax_request_to_check_user(self):
        response = self.client.post(
            '/ppw10/checker/', 
            json.dumps({
                'name' : 'name', 
                'email' : 'email@email.com',
                'password' : 'password',
            }),
            content_type="application/json", 
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        data = response.json()
        self.assertTrue(data['valid'])
        self.assertFalse(data['subscriber_exists'])

    def test_get_ajax_request_to_check_user(self):
        response = self.client.get('/ppw10/checker/')
        self.assertEqual(response.status_code, 200)
    
class SubscribeCreationTest(TestCase):
    def test_post_ajax_request_to_create_user(self):
        response = self.client.post(
            '/ppw10/subscribe/', 
            json.dumps({
                'name' : 'name', 
                'email' : 'email@email.com',
                'password' : 'password'
            }),
            content_type="application/json", 
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        self.assertEqual(response.status_code, 200)

    def test_get_ajax_request_to_create_user(self):
        response = self.client.get('/ppw10/subscribe/')
        self.assertEqual(response.status_code, 200)

