from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
from .models import Subscriber
from .forms import SubscriberForm
import json

# Create your views here.
def sub(request):
    form = SubscriberForm()
    response = {
        "form" : form
    }
    return render(request, 'lab10.html', response)

def checker(request):
    if request.method == "POST" and request.is_ajax(): #is_ajax ngecek dia ngelakuinnya dr ajax ga
        jsondata = json.loads(request.body, encoding='UTF-8') #loads itu jdnya object kl dumps kan string
        existed = Subscriber.objects.filter(email=jsondata['email']). first() #.first() return the first result of query, none kl gaada
        form = SubscriberForm({
            'name' : jsondata['name'],
            'email' : jsondata['email'],
            'password' : jsondata['password']
        })
        password_confirmed = jsondata['password'] == jsondata['password']
        return JsonResponse({
            "valid" : form.is_valid(),
            "subscriber_exists" : existed != None,
            "pass_confirmed" : password_confirmed
        })
    return HttpResponse("Test", status=200)

def subscribe(request):
    if request.method == "POST" and request.is_ajax():
        jsondata = json.loads(request.body, encoding='UTF-8')
        Subscriber.objects.create(
            name = jsondata['name'],
            email = jsondata['email'],
            password = jsondata['password']
        )
        return HttpResponse("Created", status=201)
    else:
        return HttpResponse("Test", status=200)

def subscriberList(request):
    if request.is_ajax():
        all_subscribers = Subscriber.objects.all().values()
        subscribers = list(all_subscribers)
        return JsonResponse({'all_subscribers' : subscribers})

def unsubscribe(request):
    if request.method == "POST" and request.is_ajax():
        jsondata = json.loads(request.body, encoding='UTF-8')
        user = Subscriber.objects.get(id=jsondata['id'])
        pass_confirmed = user.password == jsondata['password']
        if pass_confirmed:
            user.delete()
        return JsonResponse({
            'pass_confirmed':pass_confirmed,
        })
    return HttpResponse("Test", status=200)