from django import forms
from . import models

class SubscriberForm(forms.Form):
    name = forms.CharField(label="Name", widget=forms.TextInput(attrs={
        "class"         :"form-control",
        "required"      :True,
        "onInput"       :"validation()", #darin jangan lupa bikin fungsinya di js
        "placeholder"   :"John Doe"
    }), max_length=254)
    email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={
        "class"         :"form-control",
        "required"      :True,
        "onInput"       :"validation()", #darin jangan lupa bikin fungsinya di js
        "placeholder"   :"johndoe@mail.com"
    }), max_length=50)
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={
        "class"         :"form-control",
        "required"      :True,
        "onInput"       :"validation()", #darin jangan lupa bikin fungsinya di js
        "placeholder"   :"Write Your Password"
    }), max_length=50)

    class Meta:
        model = models.Subscriber
 