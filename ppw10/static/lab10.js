function listUser(){
    $.ajax({
        url: '/ppw10/subscriber-list/',
        datatype: 'json',
        success: function(data){
            $('tbody').html('')
            var result ='<tr>';
            for(var i = 0; i < data.all_subscribers.length; i++) {
                result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td class='align-middle text-center'>" + data.all_subscribers[i].name +"</td>" +
                "<td class='align-middle text-center'>" + data.all_subscribers[i].email +"</td>" +
                "<td class='align-middle text-center'>" + "<a " + "data-email=" + data.all_subscribers[i].email + 
                " class='text-white btn btn-danger unsubscribe-button' float-right role='button' aria-pressed='true' onclick=validate_unsubscriber(this.id) id="+ i +">" + "Unsubscribe" + "</a></td></tr>";
            }
            $('tbody').append(result);
            // let unsub_button = document.getElementsByClassName("unsubscribe-button");
            //     for (i = 0; i < unsub_button.length; i++) {
            //         unsub_button[i].addEventListener("click", function() {
            //             validate_unsubscriber(data.all_subscribers[i].id);
            //         })
            //     }
            },
        error: function(error){
            alert(":(");
        }
    })
}

function validation(){
    event.preventDefault(true);
    let fields = document.forms["subscriber_form"].getElementsByTagName("input");
    let csrf_token = fields[0].value;
    let name_input = fields[1].value;
    let email_input = fields[2].value;
    let password_input = fields[3].value;
    $.ajax({
        url: '/ppw10/checker/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            name : name_input,
            email : email_input,
            password : password_input
        }),
        beforeSend: function(request){
            request.setRequestHeader('X-CSRFTOKEN', csrf_token);
        },
        success: function(result){
            let alerttext = document.getElementById("alerttext");
            let submitbutton = document.getElementById("submitbutton");
            alerttext.classList.remove("red-text", "green-text");
            if(result['valid']){
                if(result['subscriber_exists']){
                    submitbutton.disabled = true;
                    alerttext.classList.add("red-text");
                    alerttext.textContent = "A user with this email address already exists.";
                }else{
                    submitbutton.disabled = false;
                    alerttext.classList.add("green-text");
                    alerttext.textContent = "Available! :)";
                }
            }else if(!result['valid']){
                submitbutton.disabled = true;
                alerttext.classList.add("red-text");
                alerttext.textContent = "There are some errors, please correct them!";
            }
        }
    });
}

function userclick(){
    event.preventDefault(true);
    let form = document.forms["subscriber_form"]
    let fields = form.getElementsByTagName("input");
    let csrf_token = fields[0].value;
    let name_input = fields[1].value;
    let email_input = fields[2].value;
    let password_input = fields[3].value;
    $.ajax({
        url: '/ppw10/subscribe-success/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            name : name_input,
            email : email_input,
            password : password_input,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token);
        },
        complete: function(result) {
            let alerttext = document.getElementById("alerttext");
            alerttext.classList.remove("red-text", "green-text");
            alerttext.classList.add("green-text");
            alerttext.innerHTML = "Thank you for subscribing!";
            let submitbutton = document.getElementById("submitbutton");
            submitbutton.disabled = true;
            form.reset();
            listUser();
        }
    });
}


function validate_unsubscriber(user_id){
    console.log("coba")
    var password = prompt("Please enter your password! :", "");
    if(password != null && password != ""){
        unsubscribe(user_id, password);
    }
}

function unsubscribe(user_id, password_input){
    let csrf_token = $('input[name="csrfmiddlewaretoken"]').attr('value');
    $.ajax({
        url: '/ppw10/unsubscribe/',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify({
            id : user_id,
            password : password_input,
        }),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', csrf_token);
        },
        success: function(result) {
            if(result['pass_confirmed']){
                alert("Unsubscribed");
            } else {
                alert("Wrong password");
            }
        }
    });
}
