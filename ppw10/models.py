from django.db import models

# Create your models here.

class Subscriber(models.Model):
    name = models.CharField(max_length=254)
    email = models.EmailField(max_length=50)
    password = models.CharField(max_length=50)