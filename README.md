# LAB PPW Fasilkom UI 2018

## Data Diri
Nama : Darin Amanda Zakiya <br />
NPM : 1706979190 <br />
Kelas : PPW- B

## Status Aplikasi
[![pipeline status](https://gitlab.com/darinamanda/ppwlab/badges/master/pipeline.svg)](https://gitlab.com/darinamanda/ppwlab/commits/master)
[![coverage report](https://gitlab.com/darinamanda/ppwlab/badges/master/coverage.svg)](https://gitlab.com/darinamanda/ppwlab/commits/master)

## Link Heroku App
https://ppw-darina.herokuapp.com